<?php
namespace Axe\Http;

use Axe\Api\Credentials;

class Request
{
    const API_URL = 'http://www.datdota.com/axe/';

    /** @var Credentials */
    protected $credentials;

    /** @var string */
    protected $endPoint;

    /**
     * Initialize a request object
     * @param Credentials $credentials
     * @param $endPoint
     */
    public function __construct(Credentials $credentials, $endPoint)
    {
        $this->setCredentials($credentials);
        $this->setEndPoint($endPoint);
    }

    /**
     * Set the credentials necessary to make API calls
     * @param Credentials $credentials
     * @return $this
     */
    public function setCredentials(Credentials $credentials)
    {
        $this->credentials = $credentials;
        return $this;
    }

    /**
     * Set the API end point to be called
     * @param string $endPoint
     * @return $this;
     */
    public function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;
        return $this;
    }

    /**
     * Sends a GET request to the API end point and returns the value
     * @param array $params
     * @return string
     */
    public function get(array $params = array())
    {
        $params = array_merge($params, array(
            'user' => $this->credentials->getApiUser(),
            'key' => $this->credentials->getApiKey()
        ));

        $queryString = '?' . http_build_query($params);
        $url = self::API_URL . $this->endPoint . $queryString;

        if (function_exists('curl_init')) {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);

            curl_close($ch);
        }
        else {
            $response = file_get_contents($url);
        }

        return $response;
    }

}