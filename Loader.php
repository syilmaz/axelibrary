<?php
namespace Axe;

/**
 * Loader class to automatically load the correct files.
 * @package Axe
 */
class Loader
{
    /** @var boolean Store whether the auto loader has already been registered */
    protected $registered;

    /**
     * Registers the auto load to start loading classes
     * @return $this
     */
    public function register()
    {
        // We have already registered
        if ($this->registered) {
            return $this;
        }

        // Register the load method to auto load
        $this->registered = spl_autoload_register(array($this, 'load'));

        return $this;
    }

    /**
     * Triggered when a class needs to be loaded
     * @param $className
     */
    protected function load($className)
    {
        $parts = explode('\\', $className);

        // Remove the first part since it's identical to our current namespace
        if (strtolower(reset($parts)) == strtolower(__NAMESPACE__)) {
            array_shift($parts);
        }

        require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $parts) . '.php';
    }

    /**
     * Unregisters the auto load method
     * @return $this
     */
    public function unregister()
    {
        // We're not registered so there is no point in unregistering
        if (!$this->registered) {
            return $this;
        }

        $this->registered = !spl_autoload_unregister(array($this, 'load'));

        return $this;
    }

}