<?php
namespace Axe\Api;

use Axe\Http\Request;
use Axe\Model\Player as Model_Player;

class Player
{
    /** @var Credentials */
    protected $credentials;

    /**
     * @param Credentials $credentials The API credentials for calls
     */
    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
    }

    public function getStats($playerId, $limit = 100, $id_min = null, $id_max = null, $event_id = null)
    {
        $endPoint = 'getPlayerStats.php';

        $request = new Request($this->credentials, $endPoint);

        $params = array(
            'player_id' => $playerId,
            'limit'     => $limit,
            'id_min'    => $id_min,
            'id_max'    => $id_max,
            'event'  => $event_id
        );

        $response = $request->get(array_filter($params));

        $data = @json_decode($response, true);

        if ($data == null) {
            throw new \Exception($response);
        }
    }

    /**
     * Retrieve an array of players
     * @return array Array of Player objects
     * @throws \Exception
     */
    public function getList()
    {
        $endPoint = 'getPlayerList.php';

        $request = new Request($this->credentials, $endPoint);
        $response = $request->get();

        $data = @json_decode($response, true);

        if ($data == null) {
            throw new \Exception($response);
        }

        $list = array();

        foreach ($data as $row) {
            $player = new Model_Player();
            $player->setData($row);

            $list []= $player;
        }

        return $list;
    }

}