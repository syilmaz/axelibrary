<?php
namespace Axe\Api\Helper;

class Patch
{
    /**
     * Retrieve the patch version based on the match id
     * @param $matchId
     * @return string
     */
    public static function getVersionFromMatchId($matchId)
    {
        if ($matchId > 1097824357) {
            return "6.83";
        }
        elseif ($matchId > 920489420) {
            return "6.82";
        }
        elseif ($matchId > 634455781) {
            return "6.81";
        }
        elseif ($matchId > 490145178) {
            return "6.80";
        }
        elseif ($matchId > 353805306) {
            return "6.79";
        }
        elseif ($matchId > 211765788) {
            return "6.78";
        }
        elseif ($matchId > 81640062) {
            return "6.77";
        }
        elseif ($matchId > 53113898) {
            return "6.76";
        }
        else {
            return "6.75";
        }
    }
}