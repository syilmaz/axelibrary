<?php
namespace Axe\Api;

class Credentials
{
    /** @var string */
    private $apiUser;

    /** @var string */
    private $apiKey;

    public function __construct($apiUser, $apiKey)
    {
        $this->setApiUser($apiUser);
        $this->setApiKey($apiKey);
    }

    /**
     * Change the API user
     * @param $apiUser
     * @return $this
     */
    public function setApiUser($apiUser)
    {
        $this->apiUser = $apiUser;
        return $this;
    }

    /**
     * Change the API key
     * @param $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * Returns the API user
     * @return string
     */
    public function getApiUser()
    {
        return $this->apiUser;
    }

    /**
     * Returns the API key
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
}