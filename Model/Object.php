<?php
namespace Axe\Model;

abstract class Object
{
    /** @var array */
    protected $data = array();

    /**
     * Set the data to this object
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Returns the raw data object
     * @return array
     */
    public function debug()
    {
        return $this->data;
    }

    public function __call($name, $arguments)
    {
        if (strpos($name, 'get') === 0) {
            $name = preg_replace('#^get#', '', $name);
            $name = strtolower(preg_replace('#([A-Z])#', '_$1', lcfirst($name)));

            if (!isset($this->data[$name])) {
                return null;
            }

            return $this->data[$name];
        }
        else if (strpos($name, 'set') === 0) {

            $name = preg_replace('#^set#', '', $name);
            $name = strtolower(preg_replace('#([A-Z])#', '_$1', lcfirst($name)));

            $this->data[$name] = reset($arguments);
            return $this;
        }

        return $this;
    }
}