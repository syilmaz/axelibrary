<?php
namespace Axe\Model;

/**
 * Class Player
 * @package Axe\Model
 *
 * @method string getPlayerId
 * @method void setPlayerId($playerId)
 * @method string getName
 * @method void setName($name)
 * @method string getTeamId
 * @method void setTeamId($teamId)
 */
class Player extends Object
{

}